﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private readonly IRepository<PromoCode> _promocodeRepository;
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<Preference> _preferenceRepository;


        public PromocodesController(IRepository<PromoCode> promocodeRepository, IRepository<Customer> customerRepository, IRepository<Employee> employeeRepository, IRepository<Preference> preferenceRepository)
        {
            _promocodeRepository = promocodeRepository;
            _customerRepository = customerRepository;
            _employeeRepository = employeeRepository;
            _preferenceRepository = preferenceRepository;
        }

        /// <summary>
        /// Получить все промокоды
        /// </summary>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            //TODO: Получить все промокоды 
            var promocodes = await _promocodeRepository.GetAllAsync();

            var shortPromocodes = promocodes.Select(x => new PromoCodeShortResponse
            {
                BeginDate = x.BeginDate.ToShortDateString(),
                EndDate = x.EndDate.ToShortDateString(),
                Code = x.Code,
                ServiceInfo = x.ServiceInfo,
                Id = x.Id,
                PartnerName = x.PartnerName
            });

            return Ok(shortPromocodes);
        }

        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            var preferences = await _preferenceRepository.GetAllAsync();
            var preference = preferences.FirstOrDefault(x => x.Name == request.Preference);
            if (preference == null)
                return BadRequest();

            var employees = await _employeeRepository.GetAllAsync();
            var employee = employees.FirstOrDefault(x => x.FullName == request.PartnerName);
            if (employee == null)
                return BadRequest();

            try
            {
                PromoCode promoCode = new PromoCode()
                {
                    ServiceInfo = request.ServiceInfo,
                    PartnerName = request.PartnerName,
                    Code = request.PromoCode,
                    Preference = preference,
                    BeginDate = DateTime.ParseExact(request.BeginDate, "yyyy-MM-dd", null),
                    EndDate = DateTime.ParseExact(request.EndDate, "yyyy-MM-dd", null),
                    Id = Guid.NewGuid(),
                    PartnerManagerId = employee.Id

                };


            var customers = await _customerRepository.GetAllAsync();
            foreach(var customer in customers)
            {
                if (customer.Preferences.FirstOrDefault(x => x.PreferenceId == preference.Id) != null) 
                {
                    promoCode.Customer = customer;
                    customer.PromoCodes.Add(promoCode);
                    break;
                }
            }
               await _promocodeRepository.AddAsync(promoCode);


                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}