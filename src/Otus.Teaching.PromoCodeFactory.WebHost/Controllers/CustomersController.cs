﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<PromoCode> _promocodeRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public CustomersController(IRepository<Customer> customerRepository, IRepository<PromoCode> promocodeRepository, IRepository<Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _promocodeRepository = promocodeRepository;
            _preferenceRepository = preferenceRepository;
        }

        /// <summary>
        /// Получить всех клиентов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<CustomerShortResponse>> GetCustomersAsync()
        {
            var customers = await _customerRepository.GetAllAsync();
            var response = customers.Select(x => new CustomerShortResponse(x)).ToList();
            return Ok(response);
        }
        
        /// <summary>
        /// Получить клиента по id, с промокодами
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            CustomerResponse response;
            try
            {
                var customer = await _customerRepository.GetByIdAsync(id);
                if (customer == null)
                    return NotFound();

                var promocodes = await _promocodeRepository.GetAllAsync();
                var selected = 

                response = new CustomerResponse
                {
                    Id = customer.Id,
                    FirstName = customer.FirstName,
                    LastName = customer.LastName,
                    Email = customer.Email,
                    Preferences = customer.Preferences.Select(c => new PreferenceResponse { Name = c.Preference.Name, Id = c.PreferenceId }).ToList(),
                    PromoCodes = promocodes.Select(p => new PromoCodeShortResponse
                    {
                        Id = p.Id,
                        BeginDate = p.BeginDate.ToShortDateString(),
                        EndDate = p.EndDate.ToShortDateString(),
                        Code = p.Code,
                        PartnerName = p.PartnerName,
                        ServiceInfo = p.ServiceInfo,
                    }).ToList()
                };
                


            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

            return Ok(response);


        }

        /// <summary>
        /// Создание нового клиента
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            var customerId = Guid.NewGuid();
            var customer = new Customer()
            {
                Id = customerId,
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
                Preferences = request.PreferenceIds.Select(id => new CustomerPreference { CustomerId = customerId,  PreferenceId = id }).ToList()
            };

            await _customerRepository.AddAsync(customer);

            return Ok();
        }
        /// <summary>
        /// Обновить данные клиента
        /// </summary>
        /// <param name="id"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            if(customer == null) 
                return NotFound();

            var preferences = await GetPreferencesAsync(request.PreferenceIds);

            if (customer == null)
            {
                customer = new Customer();
            }

            customer.Email = request.Email;
            customer.FirstName = request.FirstName;
            customer.LastName = request.LastName;

            if (preferences != null && preferences.Any())
            {
                customer.Preferences?.Clear();
                customer.Preferences = preferences.Select(x => new CustomerPreference()
                {
                    Customer = customer,
                    Preference = x,
                    CustomerId = customer.Id,
                    PreferenceId = x.Id,

                }).ToList();
            }

            await _customerRepository.UpdateAsync(customer);

            return Ok();          

        }

        /// <summary>
        /// Удалить клиента
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            //TODO: Удаление клиента вместе с выданными ему промокодами
            var customer = await _customerRepository.GetByIdAsync(id);
            if(customer == null)
                return NotFound();

            var promocodes = customer.PromoCodes;
            foreach (var promocode in promocodes) 
            {
               await _promocodeRepository.DeleteAsync(promocode);
            }

            await _customerRepository.DeleteAsync(customer);
            return Ok();
        }

        private Task<IEnumerable<Preference>> GetPreferencesAsync(IEnumerable<Guid> ids)
        {
            IEnumerable<Preference> preferences = new List<Preference>();
            if (ids != null && ids.Any())
            {
                //Получаем предпочтения из бд и сохраняем большой объект
                return _preferenceRepository
                    .GetRangeByIdsAsync(ids.ToList());
            }

            return Task.FromResult(preferences);
        }

    }
}