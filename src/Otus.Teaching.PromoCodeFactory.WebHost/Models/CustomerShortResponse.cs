﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class CustomerShortResponse
    {
        public CustomerShortResponse(Customer x)
        {
            Id = x.Id;
            FirstName = x.FirstName;
            LastName = x.LastName;
            Email = x.Email;
        }

        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
    }
}