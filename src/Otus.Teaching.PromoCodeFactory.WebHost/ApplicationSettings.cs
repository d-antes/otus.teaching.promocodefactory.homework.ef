﻿namespace Otus.Teaching.PromoCodeFactory.WebHost
{
    public class ApplicationSettings
    {
        public string ConnectionString { get; set; }
    }
}
