﻿namespace Otus.Teaching.PromoCodeFactory.DataAccess.Context
{
    public interface IDbInitializer
    {
        void Initialize();
    }
}