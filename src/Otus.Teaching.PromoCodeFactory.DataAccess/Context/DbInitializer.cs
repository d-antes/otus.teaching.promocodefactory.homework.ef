﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;


namespace Otus.Teaching.PromoCodeFactory.DataAccess.Context
{
    public class DbInitializer:IDbInitializer
    {
        protected readonly DataContext _dataContext;

        public DbInitializer(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public void Initialize()
        {
            _dataContext.Database.Migrate();
           // _dataContext.Database.EnsureDeleted();
           // _dataContext.Database.EnsureCreated();


            _dataContext.AddRange(FakeDataFactory.Employees);
            _dataContext.AddRange(FakeDataFactory.Roles);
            _dataContext.SaveChanges();

            _dataContext.AddRange(FakeDataFactory.Preferences);
            _dataContext.SaveChanges();


            _dataContext.AddRange(FakeDataFactory.Customers);
            _dataContext.SaveChanges();

            _dataContext.AddRange(FakeDataFactory.PromoCodes);
            _dataContext.SaveChanges();
        }
    }
}
