﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Context
{
    public class DataContext:DbContext
    {
        public DbSet<PromoCode> PromoCodes { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Preference> Preferences { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DataContext()
        {

        }
        public DataContext(DbContextOptions<DataContext> options):base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Employee>().Property(e => e.FirstName).HasMaxLength(50);
            modelBuilder.Entity<Employee>().Property(e => e.LastName).HasMaxLength(50);
            modelBuilder.Entity<Employee>().Property(e => e.Email).HasMaxLength(255);

            modelBuilder.Entity<Preference>().Property(p => p.Name).HasMaxLength(50);

            modelBuilder.Entity<PromoCode>().Property(pm => pm.Code).HasMaxLength(30);
            modelBuilder.Entity<PromoCode>().Property(pm => pm.ServiceInfo).HasMaxLength(255);
            modelBuilder.Entity<PromoCode>().Property(pm => pm.PartnerName).HasMaxLength(50);

            modelBuilder.Entity<Role>().Property(r => r.Name).HasMaxLength(50);
            modelBuilder.Entity<Role>().Property(r => r.Description).HasMaxLength(255);

            modelBuilder.Entity<Customer>().Property(c => c.FirstName).HasMaxLength(50);
            modelBuilder.Entity<Customer>().Property(c => c.LastName).HasMaxLength(50);
            modelBuilder.Entity<Customer>().Property(c => c.Email).HasMaxLength(255);

            modelBuilder.Entity<CustomerPreference>()
                         .HasKey(bc => new { bc.CustomerId, bc.PreferenceId });
            modelBuilder.Entity<CustomerPreference>()
                .HasOne(bc => bc.Customer)
                .WithMany(b => b.Preferences)
                .HasForeignKey(bc => bc.CustomerId);
            modelBuilder.Entity<CustomerPreference>()
                .HasOne(bc => bc.Preference)
                .WithMany()
                .HasForeignKey(bc => bc.PreferenceId);

            modelBuilder.Entity<Customer>()
                .HasMany(p=>p.PromoCodes)
                .WithOne(c => c.Customer);
        }
    }
}
